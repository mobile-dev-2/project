import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _Formkey = GlobalKey<FormState>();
  String name = '';
  String gender = 'M'; //M,F
  int age = 0;
  int weigth = 0;

  final _nameContorller = TextEditingController();
  final _ageContorller = TextEditingController();
  final _weigthContorller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameContorller.text = name;
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      _ageContorller.text = '$age';
      weigth = prefs.getInt('weigth') ?? 0;
      _weigthContorller.text = '$weigth';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        backgroundColor: Colors.black12,
      ),
      body: Form(
        key: _Formkey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Text('$name $gender $age'),
              TextFormField(
                autofocus: true,
                controller: _nameContorller,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'กรุณาใส่ข้อมูลชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ชื่อ-นามสกุล'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              TextFormField(
                controller: _ageContorller,
                validator: (value) {
                  var isNum = int.tryParse(value!);
                  if (isNum == null || int.parse(value) <= 0) {
                    return 'กรุณาใส่ที่มากกว่า 0 ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'อายุ'),
                onChanged: (value) {
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _weigthContorller,
                validator: (value) {
                  var isNum = int.tryParse(value!);
                  if (isNum == null || int.parse(value) <= 0) {
                    return 'กรุณาใส่ที่มากกว่า 0 ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'น้ำหนัก'),
                onChanged: (value) {
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              DropdownButtonFormField(
                value: gender,
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        SizedBox(width: 8.0),
                        Text('ชาย')
                      ],
                    ),
                    value: 'M',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        SizedBox(width: 8.0),
                        Text('หญิง')
                      ],
                    ),
                    value: 'F',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    gender = newValue!;
                  });
                },
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_Formkey.currentState!.validate()) {
                      _saveProfile();
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save'))
            ],
          ),
        ),
      ),
    );
  }
}
