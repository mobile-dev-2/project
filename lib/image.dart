import 'package:flutter/material.dart';

class imageWidget extends StatefulWidget {
  imageWidget({Key? key}) : super(key: key);

  @override
  _imageWidgetState createState() => _imageWidgetState();
}

class _imageWidgetState extends State<imageWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('รูปที่น่ารัก'),
        backgroundColor: Colors.black12,
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Row(
                    children: [
                      Image.asset(
                        'image/im01.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'image/im02.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Image.asset(
                        'image/im03.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'image/im04.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Image.asset(
                        'image/im05.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'image/im06.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Image.asset(
                        'image/im07.jpg',
                        width: 400,
                        height: 400,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
