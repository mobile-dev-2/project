import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questionnValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var questions = [
    'ถ่ายพยาธิ',
    'วัคซีนป้องกันพิษสุนัขบ้า',
    'วัคซีนป้องกันโรคหลอดลม',
    'ยาป้องกันพยาธิภายใน',
    'ยาป้องกันพยาธิหนอนหัวใจ',
    'วัคซีนป้องกันหวัดสุนัข',
    'ยาลำไส้อักเสบ',
    'ยาตับอักเสบ',
    'เลปโตรไปโรซิส',
    'ฟาราอินฟูลเอนซ่า',
    'ยาแก้ท้องร่วง'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
        backgroundColor: Colors.black12,
      ),
      body: ListView(
        children: [
          CheckboxListTile(
            value: questionnValue[0],
            title: Text(questions[0]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[0] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[1],
            title: Text(questions[1]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[1] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[2],
            title: Text(questions[2]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[2] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[3],
            title: Text(questions[3]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[3] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[4],
            title: Text(questions[4]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[4] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[5],
            title: Text(questions[5]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[5] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[6],
            title: Text(questions[6]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[6] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[7],
            title: Text(questions[7]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[7] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[8],
            title: Text(questions[8]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[8] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[9],
            title: Text(questions[9]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[9] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionnValue[10],
            title: Text(questions[10]),
            onChanged: (newValue) {
              setState(() {
                questionnValue[10] = newValue!;
              });
            },
          ),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false, false, false, false, false, false, false, false, false, false, false]";
    var arrStrQuestionValue =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValue.length; i++) {
        questionnValue[i] = (arrStrQuestionValue[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionnValue.toString());
  }
}
