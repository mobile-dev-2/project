import 'package:covid_app/image.dart';
import 'package:covid_app/profile.dart';
import 'package:covid_app/question.dart';
import 'package:covid_app/radio_widget.dart';
import 'package:covid_app/risk.dart';
import 'package:covid_app/vaccine.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'DOG STORY',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M'; //M,F
  int age = 0;
  var vaccines = ['-', '-', '-'];
  var questionScore = 0.0;
  var riskScore = 0.0;
  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('vaccines');
    prefs.remove('question_values');
    prefs.remove('risk_values');
    await _loadProfile();
    await _loadQuestion();
    await _loadRisk();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DOG STORY'),
        backgroundColor: Colors.black12,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text(''),
              decoration: BoxDecoration(color: Colors.black12),
            ),
            ListTile(
              title: Text('โปรไฟล'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfileWidget()));
                await _loadProfile();
              },
            ),
            ListTile(
              title: Text('ประวัติวัคซีน'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => QuestionWidget()));
                await _loadQuestion();
              },
            ),
            ListTile(
              title: Text('กลุ่มโรคเสี่ยง'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RiskionWidget()));
                await _loadRisk();
              },
            ),
            ListTile(
              title: Text('รูปที่น่ารัก'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => imageWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            )
          ],
        ),
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading:
                      Image.asset('image/icon.png'), //ใส่รูปจะได้มีความเป็นหมา
                  title: Text(name),
                  subtitle: Text(
                    'เพศ: $gender , อายุ: $age ปี',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                      'วัคซีนที่ฉีด: ${questionScore.toStringAsFixed(2)}%',
                      style: TextStyle(
                          color: (questionScore > 60)
                              ? Colors.green.withOpacity(0.6)
                              : Colors.black.withOpacity(0.6),
                          fontSize: 24.0)),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('ความเสี่ยง: ${riskScore.toStringAsFixed(2)}%',
                      style: TextStyle(
                          color: (riskScore > 25)
                              ? Colors.red.withOpacity(0.6)
                              : Colors.black.withOpacity(0.6),
                          fontSize: 24.0)),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(16.0),
                //   child: Text('ได้รับการฉีด ${vaccines.toString()}',
                //       style: TextStyle(color: Colors.black)),
                // ),
                ButtonBar(
                  children: [
                    TextButton(
                        onPressed: () async {
                          await _resetProfile();
                        },
                        child: const Text('Del'))
                  ],
                )
              ],
            ),
          ),
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Image.asset(
                    'image/icon.png',
                    color: Colors.blue,
                  ),
                  title: Text('ทิฟฟี่'),
                  subtitle: Text(
                    'เพศ: เมีย , อายุ: 6 ปี',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'วัคซีนที่ฉีด: 100%',
                    style: TextStyle(
                        fontSize: 24.0, color: Colors.green.withOpacity(0.6)),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(16.0),
                //   child: Text('ได้รับการฉีด ${vaccines.toString()}',
                //       style: TextStyle(color: Colors.black)),
                // ),
                ButtonBar(
                  children: [
                    TextButton(
                        onPressed: () async {
                          await _resetProfile();
                        },
                        child: const Text('Del'))
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false, false, false, false, false, false, false, false, false, false, false]";
    var arrStrQuestionValue =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValue.length; i++) {
        if (arrStrQuestionValue[i].trim() == 'true') {
          questionScore++;
        }
      }
      questionScore = 100 * questionScore / 11;
    });
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_values') ??
        "[false, false, false, false, false, false, false]";
    var arrStrRiskValue =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      riskScore = 0.0;
      for (var i = 0; i < arrStrRiskValue.length; i++) {
        if (arrStrRiskValue[i].trim() == 'true') {
          riskScore++;
        }
      }
      riskScore = 100 * riskScore / 7;
    });
  }
}
