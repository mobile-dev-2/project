import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskionWidget extends StatefulWidget {
  RiskionWidget({Key? key}) : super(key: key);

  @override
  _RiskionWidgetState createState() => _RiskionWidgetState();
}

class _RiskionWidgetState extends State<RiskionWidget> {
  var riskValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var risks = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('โรคเสี่ยง'),
        backgroundColor: Colors.black12,
      ),
      body: ListView(
        children: [
          CheckboxListTile(
            value: riskValue[0],
            title: Text(risks[0]),
            onChanged: (newValue) {
              setState(() {
                riskValue[0] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: riskValue[1],
            title: Text(risks[1]),
            onChanged: (newValue) {
              setState(() {
                riskValue[1] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: riskValue[2],
            title: Text(risks[2]),
            onChanged: (newValue) {
              setState(() {
                riskValue[2] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: riskValue[3],
            title: Text(risks[3]),
            onChanged: (newValue) {
              setState(() {
                riskValue[3] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: riskValue[4],
            title: Text(risks[4]),
            onChanged: (newValue) {
              setState(() {
                riskValue[4] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: riskValue[5],
            title: Text(risks[5]),
            onChanged: (newValue) {
              setState(() {
                riskValue[5] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: riskValue[6],
            title: Text(risks[6]),
            onChanged: (newValue) {
              setState(() {
                riskValue[6] = newValue!;
              });
            },
          ),
          ElevatedButton(
              onPressed: () async {
                await _saveRisk();
                Navigator.pop(context);
              },
              child: Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_values') ??
        "[false, false, false, false, false, false, false]";
    var arrStrRiskValue =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrRiskValue.length; i++) {
        riskValue[i] = (arrStrRiskValue[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', riskValue.toString());
  }
}
